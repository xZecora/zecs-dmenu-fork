# xZecora's dmenu fork

Patches applied include:
- alpha
- fuzzy highlight
- fuzzy match
- border
- center

# Requirements
Install the patched version of libxft-bgra
